# Scripts

## Création des datasets
Un ensemble de scripts qui ont permis la création de datasets

## Conversion

Un ensemble de scripts qui offrent des vues différentes:
- tableaux csv
- arbres json
- graphes (gexf)

## Manipulation de données
 Un ensemble de scripts qui ont permis de croiser des datasets ensemble:
par exemple les arguments et les participants...

