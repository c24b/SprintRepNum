#/usr/bin/python3
# coding: utf-8

import os
from pandas.io.json import json_normalize
import pandas as pd
import requests
import csv
from bs4 import BeautifulSoup as bs
from datasets_tool import write_to_csv, write_to_json

def read_csv(csvfile, sep="\n"):
    with open(csvfile, "r") as f:
        r = csv.reader(f, delimiter=sep)
        return [row for row in r]

if __name__ == "__main__":
    #les différents dossiers où sont stockés les fichiers
    current_path = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    dataset_dir = os.path.join(current_path, "datasets") 
    article_dir = os.path.join(dataset_dir, "articles")
    votes = os.path.join(dataset_dir, "id_articles.csv")
    
    OPINION_URL = "https://www.republique-numerique.fr/api/opinions/"
    articles =[]
    for row in read_csv(votes,"\t"):
        #print row
        _id, link, title = row[0], row[1], row[2]
        answer_f = {"id":_id, "article_link":link, "title":title}
        r = requests.get(OPINION_URL+_id)
        opinion =  r.json()["opinion"]
        answer = opinion["answer"]
        if answer is None:
            pass
        else:
            #[u'body', u'author', u'title']
            #print answer.keys()
            answer_f["answer_to"] = (opinion["author"]["uniqueId"]).encode("utf-8")
            answer_f["answer_title"] = (bs(answer["title"]).text).encode("utf-8")
            answer_f["answer_text"] = " ".join([(n.text).encode("utf-8") for n in bs(answer["body"]).find_all("p")])
            links = [n.get("href") for n in bs(answer["body"]).find_all("a")]
            answer_f["answer_refs"] = "***".join([n for n in links if not n.startswith("/profile/")])
            answer_f["cited_users"] = "***".join([n.replace("/profile/", "") for n in links if n.startswith("/profile/")])
            articles.append(answer_f)
            
    answer_file = os.path.join(dataset_dir, "reponses.csv")
    write_to_csv(articles, answer_file)
    
