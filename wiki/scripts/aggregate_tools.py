#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Outils pour croiser les données
"""

__author__ = 'Constance de Quatrebarbes (4barbes@gmail.com)'
__copyright__ = 'Copyright (c) 2015 Constance de Quatrebarbes'
__license__ = 'GPLv3'
__vcs_id__ = '$Id$'
__version__ = '0.0.1'

import os
import requests
requests.adapters.DEFAULT_RETRIES = 5

from bs4 import BeautifulSoup as bs

from tools import *
#API_URL = "https://www.republique-numerique.fr/api/opinions/"
current_path = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
dataset_dir = os.path.join(current_path, "datasets")

articles_dir = os.path.join(dataset_dir, "articles")
profile_dir = os.path.join(dataset_dir, "participants")
sources_dir = os.path.join(dataset_dir, "sources")
arguments_dir = os.path.join(dataset_dir, "arguments")
votes_dir = os.path.join(dataset_dir, "votes")
versions_dir = os.path.join(dataset_dir, "versions")
historique = os.path.join(dataset_dir, "historique")
fichier_articles = os.path.join(dataset_dir, "articles.json")

def format_articles():
    '''formattage des articles pour recevoir les votes et normaliser les url de chaque article'''
    articles_file = os.path.join(dataset_dir, "full_article_VF.json")
    articles = read_from_json(articles_file)["articles"]
    new_article = {"articles":[]}
    new_articles_file = os.path.join(dataset_dir, "articles_VF.json")
    
    ids_url = {}
    for article in articles:
        #reformater les urls des votes
        
        article["votes"] = []
        new_article["articles"].append(article)
    write_to_json(new_article, new_articles_file)
    return 
     
def create_participants_votes():
    '''
    ajouter au fichier article les votes
    '''
    #new_articles_file = os.path.join(dataset_dir, "articles_VF.json")
    #data = read_from_json(new_articles_file)
    
    participants = {"participants":[]}
    users_file = os.path.join(dataset_dir, "full_participants_VF.json")
    users_file_csv = os.path.join(dataset_dir, "full_participants_VF.csv")
    
    for n in os.listdir(profile_dir):
        f = os.path.join(profile_dir, n)
        data = read_from_json(f)
        participants["participants"].append(data)
        
    write_to_json(participants, users_file)
    return 
    
def agregate_votes1():
    url = 'http://www.republique-numerique.fr'
    new_articles_file = os.path.join(dataset_dir, "articles.json")
    data = read_from_json(new_articles_file)
    refs_articles = []
    ref_a = [(pos, y["_id"]) for pos,y in enumerate(data["articles"])]
    users_file = os.path.join(dataset_dir, "votes.csv")
    for n in data["articles"]:
        n["votes"] = []
        refs_articles.append((n["link"].split("/")[-1], n["_id"]))
        for n in n["versions"]:
            refs_articles.append((n["slug"], n["_id"]))
        
    print len(refs_articles)    
    with open(users_file, "r") as f:
        reader = csv.DictReader(f, delimiter="\t")
        for vote in reader:
            
            link = url+vote["article_link"]
            print vote["username"], vote["date"], vote["article_link"]
            
            #~ if "/blog/" in  link:
                #~ continue
            #~ try:
                #~ link, o = link.split("#")
            #~ except:
                #~ try:
                    #~ link, m = link.split("/versions/")
                #~ except:
                    #~ link = link
            #~ link = link.split("/")[-1]
            #~ #http://www.republique-numerique.fr/projects/projet-de-loi-numerique/consultation/consultation/opinions/section-1-competences-et-organisation/mise-en-place-d-un-guide-de-l-elu-en-charge-du-numerique
#~ 
            #~ 
            #~ try:
                #~ oid,link = [[i,l] for l,i in refs_articles if l == link][0]
                #~ 
                #~ pos,oid = [(oid, pos) for pos,i in ref_a if i == oid][0]
                #~ data["articles"][pos]["votes"].append(vote)
                #~ 
            #~ except IndexError:
                #~ print vote["vote_type"]
                #~ print "No match", link
                #~ notfound.append(vote)
#~ 
    #~ new_articles_file = os.path.join(dataset_dir, "articles_votes_recollement.json")
    #~ write_to_json(data, new_articles_file)
    
    
def aggregate_votes():
    "by splited url"
    new_articles_file = os.path.join(dataset_dir, "articles.json")
    data = read_from_json(new_articles_file)
    ref_a = [(pos, y["_id"]) for pos,y in enumerate(data["articles"])]
    
    #~ refs_articles = [(n["link"].split("/opinions/")[1],n["_id"]) for n in data["articles"]]
    
    #~ for n in data["articles"]:
        #~ n["votes"] = []
    #~ 
    #~ ref_a = [(pos, y["_id"]) for pos,y in enumerate(data["articles"])]
    #~ 
    #~ users_file = os.path.join(dataset_dir, "votes.csv")
    #~ 
    #~ with open(users_file, "r") as f:
        #~ reader = csv.DictReader(f, delimiter="\t")
        #~ arguments = []
        #~ sources = []
        #~ versions = []
        #~ notfound = []
        #~ for vote in reader:
            #~ link = vote["article_link"].split("/opinions/")[-1]
            #~ link = vote["article_link"]
            #~ 
            #~ 
            #~ if vote["vote_type"] == "article":
            #print vote["article_link"]
                #~ try:
                    #~ oid,link = [[i,l] for l,i in refs_articles if l == link][0]
                    #~ 
                    #~ pos,oid = [(oid, pos) for pos,i in ref_a if i == oid][0]
                    #~ data["articles"][pos]["votes"].append(vote)
                    #~ 
                #~ except IndexError:
                    #~ print vote["vote_type"]
                    #~ print "No match", link
                    #~ notfound.append(vote)
                    #~ pass
            #~ elif vote["vote_type"] == "source":
                #~ link, atype = link.split("#")
                #~ type_action, type_id = atype.split("-")
                #~ 
                #~ oid,link = [[i,l] for l,i in refs_articles if l == link][0]
                #~ pos,oid = [(oid, pos) for pos,i in ref_a if i == oid][0]
                #~ #data["articles"][pos]["votes"].append(vote)
                #~ sources.append(vote)
                #~ #pos, oid = [(oid, pos) for pos,i in ref_a if i == oid][0]
                #~ #print oid
            #~ elif vote["vote_type"] == "argument":
                #~ try:
                    #~ link, atype = link.split("#")
                    #~ type_action, type_id = atype.split("-")
                #~ 
                    #~ oid,link = [[i,l] for l,i in refs_articles if l == link][0]
                    #~ pos,oid = [(oid, pos) for pos,i in ref_a if i == oid][0]
                    #~ #data["articles"][pos]["votes"].append(vote)
                    #~ arguments.append(vote)
                #~ except IndexError:
                    #~ try:
                        #~ link, atype = link.split("/versions/")
                        #~ oid,link = [[i,l] for l,i in refs_articles if l == link][0]
                        #~ pos,oid = [(oid, pos) for pos,i in ref_a if i == oid][0]
                        #~ #data["articles"][pos]["votes"].append(vote)
                        #~ arguments.append(vote)
                    #~ except :
                        #~ notfound.append(vote)
                #~ 
            #~ elif vote["vote_type"] == "modification":
                #~ 
                #~ try:
                    #~ link, atype = link.split("/versions/")
                    #~ oid,link = [[i,l] for l,i in refs_articles if l == link][0]
                    #~ pos,oid = [(oid, pos) for pos,i in ref_a if i == oid][0]
                    #~ #data["articles"][pos]["votes"].append(vote)
                    #~ versions.append(vote)
                #~ except IndexError:
                    #~ print vote["vote_type"]
                    #~ print "NO match", link
                    #~ notfound.append(vote)
                #~ 
            #~ else:
                #~ notfound.append(vote)
                #~ pass
                        #~ sources_id = [n["_id"] for n in data["articles"][pos][type_action+"s"] if n["_id"] == type_id]
                        #~ print sources_id
                        #print match2, type_action, type_id
                    
                #~ else:
                    #~ print link, "No Match"
                #~ try:
                    #~ pos = refs_articles[vote["article_link"]]
                    #~ data["articles"][pos]["votes"].append(new_dict_user)
                #~ except KeyError:
                    #~ print vote
             
    #~ new_articles_file = os.path.join(dataset_dir, "articles_votes_croises.json")
    #~ write_to_json(data, new_articles_file)
    #~ new_articles_file = os.path.join(dataset_dir, "arguments_votes.json")
    #~ write_to_json(arguments, new_articles_file)
    #~ new_articles_file = os.path.join(dataset_dir, "sources_votes.json")
    #~ write_to_json(sources, new_articles_file)
    #~ new_articles_file = os.path.join(dataset_dir, "modification_votes.json")
    #~ write_to_json(versions, new_articles_file)
    #~ new_articles_file = os.path.join(dataset_dir, "notfound_votes.json")
    #~ write_to_json(notfound, new_articles_file)
def aggregate_info():
    '''aggregation des données de versions pour les articles'''
    
    articles = read_from_json(fichier_articles)
    articles_added = []
    for f in os.listdir(versions_dir):
        version = read_from_json(os.path.join(versions_dir, f))
        
        article = read_from_json(os.path.join(articles_dir, "article_"+f))
        article["versions"] = version["versions"]
        write_to_json(article, os.path.join(articles_dir, "article_"+f))
        #~ version_file = os.path.join(versions_dir, str(a["id"])+".json")
        #~ version = read_from_json(fichier_articles)
        #~ print version["articles"][a["_id"].keys()
        #~ break
        #~ a["versions"] = version["articles"]
        #~ a["versions_count"] = len(a["versions"])
        #~ print a.keys()
        #~ articles_added.append(a)
        #~ article_file = os.path.join(opinion_dir, "article_"+str(a["id"])+".json")
        #~ print article_file
        #~ write_to_json(a, article_file)
        
    #write_to_json({"articles":articles_added}, "new_articles.json")
def create_articles_json():
    '''tous les articles dans un seul'''
    articles = {"articles":[]}
    for f in os.listdir(articles_dir):
        if f.startswith("article_"):
            
            article = read_from_json(os.path.join(articles_dir, f))
            try:
                article["article_id"] =  article["id"]
                del article["id"]
                
            except KeyError:
                article["article_id"] = article["_id"]
                del article["_id"]
            
            args = []
            for n in article["arguments"]:
                try:
                    del n["_id"]
                except KeyError:
                    pass
                n["article_id"] = article["article_id"]
                args.append(n)
            versions = []
            for n in article["versions"]:
                n["article_id"] = article["article_id"]
                versions.append(n)
            sources = []
            for n in article["sources"]:
                try:
                    del n["_id"]
                except KeyError:
                    pass
                n["article_id"] = article["article_id"]
                sources.append(n)
            try:
                article["answer"]
            except KeyError:
                print article["article_id"], article["url"], len(article["versions"])
                break
            #~ article["answer"]["article_id"] = article["article_id"]
            #~ article["arguments"] = args
            #~ article["versions"] = versions
            #~ article["sources"] = sources
            #~ articles["articles"].append(article)
    #~ 
    #~ write_to_json({"articles":articles_added}, "new_articles.json")
        
def verify_aggregation():
    import collections
    new_articles_file = os.path.join(dataset_dir, "articles_votes_recollement.json")
    data = read_from_json(new_articles_file)
    total_manquants= []
    total_entrop = []
    total_ok=[]
    for article in data["articles"]:
        #print article["title"], article["subtitle"], 
        
        votes_declares, votes_ajoutes =  article["votes_total"],len(article["votes"])
        print (int(votes_declares) - int(votes_ajoutes))
        if votes_declares < votes_ajoutes:
            
            #~ print "N°", article["_id"],article["link"]
            #~ print "trop de votes: %i votes supplementaires" %(votes_ajoutes - votes_declares)
            total_entrop.append([article["_id"], int(votes_ajoutes-votes_declares), votes_declares])
            
            #~ users = [v["username"] for v in article["votes"]]
            #~ unique_user = set(users)
            #~ print len(users), len(set(users)), "users"
            #~ from collections import Counter
            #~ duplicate_u = [k for k,v in Counter(users).items() if v>1]
            #~ for n in duplicate_u:
                #~ votes = [v for v in article["votes"] if v["username"] == n]
                #~ print set([(n["username"], n["link"], n["date"]) for n in votes])
                #~ print article["link"]
                #~ 
                #~ print article["link"], v["link"]
                #~ print "TITRE", article["subtitle"]
                #~ print "TITRE VOTE", v["title"]
                #~ print v["link"]
            #~ break
                #~ if v["title"] != article["title"]:
                    #~ print v
                
        elif votes_declares > votes_ajoutes:
            #~ print article["_id"],article["link"]
            #~ print "pas assez de votes: %i votes Manquants" %(votes_declares -votes_ajoutes)
            total_manquants.append([article["_id"],int(votes_declares-votes_ajoutes), votes_declares])
        else:
            total_ok.append([article["_id"],votes_declares])
            pass
        #~ print "Entrop", total_entrop, sum([x[1] for x in total_entrop])
        #~ print "Enmoins", total_entrop, sum([x[1] for x in total_manquants])
        #~ print "Ok", total_ok, sum([x[1] for x in total_ok])
        
def verify_aggregation2():
    '''frregnier verif'''
    f = os.path.join(dataset_dir, "my_arts.json")
    data1 = read_from_json(f)
    print len(data1.keys())
    f = os.path.join(dataset_dir, "match_artvot.json")
    data2 = read_from_json(f)
    print len(data2.keys())
    f = os.path.join(dataset_dir, "notmatch_artvot.json")
    data3 = read_from_json(f)
    print len(data3.keys())
if __name__ == "__main__":
    #format_articles()
    #create_participants_votes()
    #aggregate_votes()
    #agregate_votes1()
    #verify_aggregation()
    #verify_aggregation2()
    #aggregate_info()
    create_articles_json()
