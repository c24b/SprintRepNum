#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Outils pour manipuler les datasets
"""

__author__ = 'Constance de Quatrebarbes (4barbes@gmail.com)'
__copyright__ = 'Copyright (c) 2015 Constance de Quatrebarbes'
__license__ = 'GPLv3'
__vcs_id__ = '$Id$'
__version__ = '0.0.1'

import os
import json, csv
import re
from pandas.io.json import json_normalize
import collections
import pandas as pd
from dataset_tools import *
from bs4 import BeautifulSoup as bs
current_path = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
dataset_dir = os.path.join(current_path, "datasets")

opinion_dir = os.path.join(dataset_dir, "opinions")
profile_dir = os.path.join(dataset_dir, "participants")
sources_dir = os.path.join(dataset_dir, "sources")
arguments_dir = os.path.join(dataset_dir, "arguments")
votes_dir = os.path.join(dataset_dir, "votes")
propositions_dir = os.path.join(dataset_dir, "propositions")
historique = os.path.join(dataset_dir, "historique")

articlesf = os.path.join(dataset_dir, "articles.json")
    
def article_stats():
    '''Vue simplifie de l'ensemble des articles'''
    # a partir du fichier des articles
    data = read_from_json(articlesf)
    articles_view = os.path.join(dataset_dir, "stats_articles.csv")
    header = [u'_id', u'cat_id', u'updated_at', u'arguments_count', u'versions_count', u'votes_total', u'sources_count', u'votes_ok', u'created_at', u'votes_mitige', u'_id', u'votes_nok']
    new_data = []
    for art in data["articles"]:
        art["article_link"] = art["link"]
        art["article_id"] = art["_id"]
        for k in (set(art.keys()) - set(header)):
            del art[k]
        
        
        new_data.append(art)
    return write_to_csv(new_data, articles_view)

def article_info():
    '''Vue simplifie de l'ensemble des articles'''
    # a partir du fichier des articles
    data = read_from_json(articlesf)
    articles_view = os.path.join(dataset_dir, "info_articles.csv")
    header = ["title", "subtitle", "body", "author"]
    new_data = []
    for art in data["articles"]:
        art["article_id"] = art["_id"]
        art["article_link"] = art["link"]
        del art["_id"]
        del art["link"]
        for k in  (set(art.keys()) - set(header)):
            del art[k]
            
        for k,v in art.items():
            try:
                art[k] = str(v).encode("utf-8")
            except:
                art[k] = clean_text((bs(v).text).encode("utf-8"))
            
        new_data.append(art)
    return write_to_csv(new_data, articles_view)
    
def arguments_view():
    '''Vue simplifie de l'ensemble des arguments'''
    articles_view = os.path.join(dataset_dir, "arguments.csv")
    data = read_from_json(articlesf)
    arguments = []
    for art in data["articles"]:
        _id = art["_id"]
        url = art["link"]
        for arg in art["arguments"]:
            arg["article_id"] = _id
            arg["article_link"] = url
            for k,v in arg.items():
                try:
                    arg[k] = str(v).encode("utf-8")
                except:
                    arg[k] = clean_text((bs(v).text).encode("utf-8"))
            arguments.append(arg)
    return write_to_csv(arguments, articles_view)
    
def votes_view():
    '''Vue simplifiée des participants'''
    votes = []
    articles_view = os.path.join(dataset_dir, "votes.csv")
    for f in os.listdir(profile_dir):
        f = os.path.join(profile_dir, f)
        user = read_from_json(f)
        #print user["username"]
        for n in user["votes"]:
            n["vote"] = str(n["vote"]).encode("utf-8")
            n["article_title"] = (bs(n["article_title"]).text).encode("utf-8")
            n["date"] = (bs(n["date"]).text).encode("utf-8")
            n["username"] = user["username"]
            
            n["user_profile"] = user["user_profile"]
            link = n["article_link"]
            
            
            if "#arg" in link:
                n["vote_type"] = "argument"
            elif "#source" in n["article_link"]:
                n["vote_type"] = "sources"
            elif "/versions/" in link:
                
                n["vote_type"] = "proposition"
            else:
                if "#" in link:
                    print link
                    break
                else:
                    n["vote_type"] = "article"
            votes.append(n)
    return write_to_csv(votes, articles_view)
        #for n in user["articles"]:
            
#~ article_stats()
#~ article_info()
#~ arguments_view()
votes_view()
