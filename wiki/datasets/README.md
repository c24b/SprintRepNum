# Datasets

L'ensemble des données de la plateforme de consultation rangées par types:
```
.
├── `arguments` Tous les arguments pour chaque article de loi
├── `articles` Tous les articles proposés au vote du projet de loi (gouvernements + participants)
├── `historique` Tous les actions sur la plateforme
├── `opinions` Tous les opinions exprimées par participants
├── `participants` Tous les actions de chaque participants
├── `projet-de-loi-numerique-consultation.csv` Le fichier en open Data des actions sur la plateforme anonymisés
├── `reponses.csv`  Les réponses du gouvernement aux propositions les plus votées (les 10)
├── `sources.csv` Toutes les sources venant illustrer les articles
├── `versions` Tous les modifications pour chaque article proposé
└── `votes` Tous les votes pour chaque article
```
Tois axes d'analyse fédèrent les datasets:
* soit une vision par action
* soit une vision par participants
* soit une vision par articles
