**Bienvenue sur l'espace collaboratif du sprint!**

Nous utilisons une plateforme collaborative et open source initialement prévue pour des dévelopeurs qu'on appelle gitlab.

Un groupe de travail a été créé spécialement à cet effet
le groupe **RepNum**

![le groupe GITLAB](./img/group_git_lab.png)

Vous pouvez nous rejoindre au plus vite en vous inscrivant
au groupe [via cette page]()

Vous consultez à présent un **projet** spécifique
appelé wiki



# **WIKI** Sprint RepNum
Ce **projet** s'appelle wiki:
c'est en effet l'espace de référence pour ces 4 jours de sprint.


Vous y trouverez:

- des **informations** pratiques, et toute la **documentation** pour le bon fonctionnement du sprint
dans l'onglet WIKI de cet espace ou en cliquant sur ce [lien](https://framagit.org/repnum/wiki/wikis/home)

- les **datasets**, **fichiers sources** et **données** mises à disposition dans le **repository**
- des **scripts** et des **outils** dans l'onglet **snippets**

Vous pouvez commencer en vous rendant sur l'espace documentation: le [wiki est par ici](https://framagit.org/repnum/wiki/wikis/home)

Vous pouvez jeter un oeil sur le **[mode d'emploi](./wiki/mode_emploi)**

## Quelques explication sur l'espace de travail ** GITLAB**


Ce gitlab est espace de **développement et de documentation collaboratif**
pour le sprint: il est organisé de la manière suivante.

**RepNum** est un [groupe](../repnum) qui réunit tous les membres du sprint RepNum:
* les [mentors](https://framagit.org/repnum/wiki/wikis/mentors) et
* les [étudiants](./https://framagit.org/groups/repnum/group_members)

La page de garde du **groupe**:

![le groupe GITLAB](./img/group_git_lab.png)

Il est constitué du plusieurs **projets**.
Pour le moment:
* wiki (l'espace de documentation et de référence)
* groupe_test (un exemple de groupe de travail)
* admin (pour la gestion interne du sprint)

Un exemple d'un **projet**:

![Exemple du projet appelé wiki](./img/exemple_projet.png)

Chaque **projet** dispose d'un dépôt (repository) pour y stocker:
* les data utilisées (repository)
* les scripts de traitement (snippets)
* la documentation (wiki)

> En fonction de votre maitrise vous pouvez aussi bien modifier les données a partir de l'interface ou a partir de la ligne de commande en *clonant le projet* auquel vous appartenez.


Vous pouvez commencer par la documentation en vous rendant sur l'espace wiki **[par ici](https://framagit.org/repnum/wiki/wikis/home)**


Bonne chance à tous et à chacun!
