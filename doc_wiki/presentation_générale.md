## Présentation

Le	dernier Sprint	de NUMI	2016-2017 est proposé	par	la plateforme CorTexT	qui fait intervenir une	équipe	de l’association HackYourPhD pour un	véritable	«	Hackathon
Recherche	» sur	 les	 données	 extraites lors	 la	 consultation	 sur	 le	 projet de loi
République	Numérique	en	décembre	2015.

Un	 véritable	 Hackathon	 privilégie	 le	 croisement	 de	 deux	 logiques	: celle	 du «	faire	»	et	celle	de	la	«	collaboration	»,	qu’il	s’agisse	de	l’usage	des	données,
du	partage	des	idées	ou	des	méthodes.
Le	Sprint	RepNum	sera	donc	pour	vous	une	opportunité	d’apprentissage	de	ce	type	de travail	 collectif	 et	 l’occasion	 de	 produire	 un	 résultat	 fondé	 sur	 l’usage	 de
données	ouvertes	pour	un	rendu	de	type	«	étude	&	recherche	».

## Objectifs

Deux	 équipes	 seront	 établies	 après	 une	 mise	 en	 commun	 des	 questions,	 des
ressources	 et	 des	 idées.	 Pour autant,	 durant ce sprint,	 des	 groupes	 de	 travail
pourront	voir	le	jour	sur	des	activités	communes	dans	un	vrai	esprit	collaboratif.
Chaque	 équipe	 aura	 pour	 objectif	 de	 produire	:
1.	 une	 *roadmap*	 de	 travail	 pour	 la
semaine	;
2.	 de	 livrer  lors du	 dernier jour	 un	 projet détaillé	 de	 site	 web
restituant	les	objectifs,	 les	questions	et	le	travail	réalisé	sur	les	données	;	et
3.	de	poursuivre	après	le	Hackathon	par	sa	réalisation	effective	pour	une	mise	en
ligne publique	et	une	évaluation	finale.

L’évaluation	concernera	donc :
* La	*RoadMap*	(1/10)
* Le	projet	de	site	(5/10)
*  Le	site	(4/10)

## Organisation

L’ [équipe	de	HackYourPhD](./mentors) organise	donc	ce	sprint,	avec	son	expérience	et	son	savoir
faire,	 et	 la	 plateforme	 CorTexT	 apportera	 un	 soutien	 et	 ses	 ressources	 tout	 au
long	de	la	semaine	(Marc	Barbier	et	Nicolas	Ricci).
Le	 programme	 de	 travail	 détaillé	 vous	 sera	 communiqué	 très	 vite.	 L’esprit	 de	 ce
sprint	 consiste	 à	 alterner	 des	 interventions	 (dont	 un	 ou	 deux	 intervenants
extérieurs),	 du	 travail	 sur	 les	 données,	 des	 moments	 de	 mise	 en	 commun	 et	 de
réflexivité.	 Par	 ailleurs,	 Clément	 Le	 Ludec	 rejoindra	 l’équipe	 d’animation	 	 la
première	journée	pour	apportera	sa	contribution.	La	dernière	journée	sera	réservée
au	montage	du	projet	de	site	web	et	à	une	présentation	de	ce	projet.
Ce	 Hackathon	 suppose	 des	 ressources	 partagées	 et	 donc	 une	 infrastructure	 de
partage.	 L’équipe	 de	 HackYourPhD	 a	 prévu	 ce	 qu’il	 fallait,	 et	 tous	 les	 accès	 aux
ressources	(data,	gitlab,	wiki)	seront	mis	à	disposition.
L’équipe	 de	 HackYourPhD	 va	 donc	 entrer	 en	 contact	 avec	 chacun	 de	 vous	 pour
apporter	 des	 informations	 et	 des	 précisions	 importantes.	 Soyez	 attentifs	 et	 pro-
actifs.	Merci	à	vous.


Marc	Barbier	(UMR	LISIS	-	CorTexT)
Producteur	du	Sprint,	Resp.	Pédagogique	pour	NUMI
