Détails des scripts

# Conditions d'accès aux données brutes

Les scripts créés par la communauté ont utilisé:
* l'api du site qui donne une vue principale de la base de données du site
* le site lui même via des méthodes de scraping

L'API donne accès à l'arborescence d'un [article] par id de l'article

ainsi que des vues spécifiques:

* des arguments pour un article
* des versions pour un article

:warning: l'accès à l'api ne donne pas l'accès à l'intégralité des informations sur les votes
seuls les 5 derniers sont disponibles.


## Scripts d'extraction et constitution des bases de référence:

Ces scripts ont permis la constitution des différents datasets et leur structuration.
Les scripts détaillent les méthodes employées et le processus de transformations de données brutes en informations
Plusieurs extraction
* [etalab](https://framagit.org/repnum/wiki/blob/master//scripts/etalab):

extraction de tous les arguments produits et leur versions

* [antoine amarilli](http://a3nm.net/git/republique/)

extraction des données pour un utilisateur

* [benjamin ooghe](https://github.com/regardscitoyens/contributions-PJLNum)
extraction des participations et des votes par articles

* [constance de quatrebarbes]
  * extraction des [articles via l'appel à l'API] (https://framagit.org/repnum/wiki/blob/master//scripts/api_tools.py)
  * extraction des [contributions  par utilisateur via l'extraction du site] (https://framagit.org/repnum/wiki/blob/master//scripts/get_participants.py)

# Scripts de formatage et de consultation

Des scripts qui structurent et organisent les données selon un schema et un mode de représentation:
en arbre (avec des noeuds) qu'il faut parcourir ou en table (avec des colonnes et des lignes et un identifiant unique qui relient les tables entre elles) dans des vues à plat

* formatage des vues à plat:

[datasets_csv](https://framagit.org/repnum/wiki/blob/master//scripts/tables_tools.py)
* ensemble d'outils pour lire les données
write_to_csv
read_from_csv
read_from_json
write_to_json
flatten
normalize
[tools](https://framagit.org/repnum/wiki/blob/master//scripts/tools.py)


# Scripts d'aggregation et de croisement:

Des routines qui aggrègent les données et croisent selon des identifiants communs.

* Des scripts de transformation d'un dataset dans un autre format:
[aggregation_tools](https://framagit.org/repnum/wiki/blob/master//scripts/agregations_tools.py)
