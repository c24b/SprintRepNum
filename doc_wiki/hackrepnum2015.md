# HackRepNum 2015

Les équipes:
* Equipe : Pour / Contre : analyse sentimentale des arguments pour faciliter l'appropriation par les utilisateurs - pourcontre-PJLNumerique

* Equipe : Tracking/act-checking - tracking-PJLNumerique

* Equipe : Découverte des "collèges de contributeurs" (lien)

* Équipe : Etude de l'évolution des votes en fonction des actions de communication de différentes organisations

* Équipe :  histoire de l'Article 8

* Equipe Regards citoyens : Communautés autour de la consultation République Numérique
