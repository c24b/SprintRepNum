## Mode d'emploi

Nous sommes sur une plateforme collaborative de documentation et de développement pour le Sprint RepNum.

C'est une plateforme et un mode de travail empruntés aux développeurs en mode agile et itératif, cet espace est amené à évoluer au cours du sprint.

Vous serez amenés à contribuer au groupe RepNum, il faut donc vous inscrire à la plateforme en tant que membre du groupe RepNum pour pouvoir participer.

Ce gitlab est un espace de **développement et de documentation collaboratif**
pour le sprint: il est organisé de la manière suivante :

**RepNum** est un [groupe](../repnum) qui réunit tous les membres du sprint RepNum:
* les [mentors](https://framagit.org/repnum/wiki/wikis/mentors) et
* les [étudiants](./https://framagit.org/groups/repnum/group_members)

Il est constitué du plusieurs projets.

Chaque projet dispose d'un dépôt (repository) pour y stocker :
* les données utilisées: **[repository](https://framagit.org/repnum/wiki/tree/master)**
* les scripts de traitement: **[snippets](https://framagit.org/repnum/wiki/snippets)**
* la documentation **[wiki](https://framagit.org/repnum/wiki/wikis.home)**

![](./organisation_espace.png)

En fonction de votre maîtrise, vous pouvez aussi bien modifier les données à partir de l'interface ou a partir de la ligne de commande en *clonant le projet* auquel vous appartenez.


## Prise en main de la plateforme

### J1

1. S'inscrire sur la plateforme et rejoindre le **[groupe](https://framagit.org/repnum/)**.

Le lien pour l'[inscription](https://framagit.org/users/sign_in?redirect_to_referer=yes) est par [ici](https://framagit.org/users/sign_in?redirect_to_referer=yes).

2. Prendre connaissance des informations pratiques (si vous êtes ici c'est que vous avez déjà lu une partie de la (doc)[./home] ;) )

3. Prendre connaissance des données disponibles sur cette plateforme individuellement
dans l'onglet **[repository](https://framagit.org/repnum/wiki/tree/master)**


### J2
Une fois les groupes constitués et les projets définis il faudra:
1. Créer un **projet**
2. **Inviter les autres membres** du groupe dans ce projet

Prenez exemple sur:
le [groupe_test](https://framagit.org/repnum/groupe_test)
3. Ajouter un Readme.md  avec les informations minimales
Puis:
4. Remplir la page Home du wiki dans l'**onglet** **wiki** et ajouter des pages en s'inspirant de l'exemple du groupe test_groupe

5. Référencez votre groupe en mettant un lien dans la page README.md du projet wiki

### Au long du sprint
Vous y êtes! Une fois, mis en route:

Vous pouvez à loisir :

6. Copier les données que vous souhaitez utiliser dans votre projet

7. Ajouter ses scripts dans l'onglet snippets :
- donner un titre explicite
- spécifier dans le nom du "File" l'extension

8. Remplir le wiki:

* Prenez exemple sur le [groupe_test](https://framagit.org/repnum/groupe_test)
* La documentation sur le wiki se fait au format *markdown*
Nous apprendrons à l'utiliser au cours du sprint...
