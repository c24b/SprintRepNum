## Equipe pédagogique

### Plateforme Cortext

* Marc Barbier responsable pédagogique
Contact : (barbier)[] marc.barbier@grignon.inra.fr
* Nicolas Ricci IT ingénieur plateforme Cortext
Contact : (nicolasricci)[] nicolas@cortext.fr


### Team HackYour PhD

* Célya Gruson Daniel (celyagd)[] celyagd@hyphd.org
co-fondatrice de HackYourPhD et doctorante en STS/SIC (UTC/Université Laval) sera là pour vous apporter un regard réflexif sur les pratiques de recherche numérique en SHS et les enjeux de l’”Open”. 

* Constance de Quatrebarbes:
Data Architect/Scientist - BnF - Promo EIG 2017. Ancienne ingénieure de la plateforme CorTexT, vous aidera à naviguer dans les données et vous initiera aux méthodes de recherche numérique.
 (c24b)[] c24b@protonmail.com
