# Précédents
## En amont de la consultation
* Etude d'impact de la consutlation faite par le cabinet Lemaire [doc](https://git.framasoft.org/c24b/republique-numerique/blob/master/datasets/2015-12-09%20Etude%20d'impact%20PJL%20vf.docx)
* Consultations du Conseil National du Numérique
https://contribuez.cnnumerique.fr/actualite/les-6-mois-de-la-concertation-nationale-%E2%80%9Cambition-num%C3%A9rique%E2%80%9D-en-data

* Etude du Conseil National du Numérique [pdf] (http://www.cnnumerique.fr/wp-content/uploads/2015/10/Avis-du-CNNum-sur-le-projet-de-loi-numerique_VF.pdf)

* Etude du Departement d'Information Scientifique et Technique du CNRS [lien vers leurs rapport] (http://www.cnrs.fr/dist/consultation-loi-numerique.html)


## En aval de la consultation

* OpenDataCamp:
Appliquer l'analyse de sentiments aux arguments exprimés sur les articles du projet de loi:

  * Constance de Quatrebarbes [https://github.com/c24b/project_loi](https://github.com/c24b/project_loi)

  * Paula Forteza: [https://git.framasoft.org/Forteza/loi-numerique](https://git.framasoft.org/Forteza/loi-numerique)
* Hackathon Recherche  Republique Numérique:

Un traitement de la consultation dans une perspective de recherche: [https://framagit.org/c24b/republique-numerique]()
