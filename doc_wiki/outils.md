## Pré-réquis

* Votre ordinateur (vous devez avoir les droits administrateur dessus)
* Une connexion Internet

## A propos de cet espace

## Exploration et manipulation de données
En fonction de vos projets vous pourrez être amenés à utiliser:

**Nous partons du principe que ces logiciels sont installés
et suffisamment maitrisés si vous choisissez de les utiliser !**

Certains outils et plateformes permettent aussi d'enquêter ou d'explorer
les données sans connaissance nécessaire particulières en 'codage'

* Cortext (http://managerv2.cortext.net)
* Iramutec (http://www.iramuteq.org/)
* OpenRefine (http://openrefine.org/)
* LibreOffice/ Excel

D'autres vous permettent une manipulation à votre guise des données
* des langage de programmation :
  * Python
  * R

Des ateliers sont prévus en fonction des besoins exprimés par les étudiants
voir les [ateliers](./ateliers)
ainsi que de la documentation complémentaire
Vous trouverez aussi des scripts d'exemples dans snippets

## Visualisation

* graphes :
  * cortext
  * gephi
  * module python de graphes (networkx graphviz...)
* cartes :
  * leaflet.js
* type powerpoint :
  * reveal.js
  * html, css
  * Powerpoint/LibreOffice

[https://ogptoolbox.org/fr/tools]

### Avancés
## Documentation

Si vous voulez pouvoir modifier les contenus du projet ou du wiki depuis la ligne de commande:
installer git
