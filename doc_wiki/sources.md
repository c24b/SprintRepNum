# Les sources

Les sources désignent le périmêtre de l'enquete autour du projet de loi sur le numérique:

* les textes de loi,
* le site de la consultation en ligne


## Textes de lois
* L'avant-projet de loi soumis à consultation du 26 septembre au 18 octobre 2015

En ligne: [propositionV0](https://www.republique-numerique.fr/pages/projet-de-loi-pour-une-republique-numerique)

En pdf
[propositionV0](../blob/master/sources/propositionV0.pdf)
* Le projet de loi transmis au conseil d'Etat le 6 novembre

En ligne: [propositionV1](https://www.republique-numerique.fr/project/projet-de-loi-numerique/step/projet-de-loi-transmis-au-conseil-d-etat)

En pdf: [propositionV1](../blob/master/sources/propositionV0.pdf)



* Le projet de loi final
transmis et validé par le conseil des ministres le 9 décembre 2015 pour vote à l'assemblée

En ligne [propositionV2](https://www.republique-numerique.fr/media/default/0001/02/ce21a30ba6d31b99c71311438a172e3c547c9dca.pdf)

En format doc [propositionV2 en format doc](https://git.framasoft.org/c24b/republique-numerique/blob/master/datasets/2015-12-09%20Projet%20de%20loi%20pr%C3%A9sent%C3%A9%20en%20CM.doc)

## La consultation
* Le site web de la consultation :
[https://www.republique-numerique.fr/consultations/projet-de-loi-numerique/consultation/consultation] (https://www.republique-numerique.fr/consultations/projet-de-loi-numerique/consultation/consultation)

* La synthèse de la consultation :
[https://www.republique-numerique.fr/project/projet-de-loi-numerique/synthesis/synthese-1](https://www.republique-numerique.fr/project/projet-de-loi-numerique/synthesis/synthese-1)

* Les réponses du gouvernement à la consultation :
[https://www.republique-numerique.fr/project/projet-de-loi-numerique/step/reponses](https://www.republique-numerique.fr/project/projet-de-loi-numerique/step/reponses)


* La charte d'utilisation du site
[https://www.republique-numerique.fr/pages/charte] (https://www.republique-numerique.fr/pages/charte)

* les mentions légales
[https://www.republique-numerique.fr/pages/mentions-legales] (https://www.republique-numerique.fr/pages/mentions-legales)
