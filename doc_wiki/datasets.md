
Les datasets désignent un **ensemble de données**
organisées qui ont fait l'objet:
* d'un premier traitement d'extraction et de structuration via des [scripts]

* d'une mise à disposition en [Open Data]

Sont distingués:
- les [**sources**](./sources.md) (ou *données brutes*) non traitées qui peuvent faire en amont l'objet d'une extraction ou d'une analyse. Elles sont recensées dans la page [sources](./sources.md) et disponible au téléchargement depuis le repository dans le dossier sources

- des **datasets** (ensemble de données organisées) qui ont *déjà* fait l'objet d'une série  d'opérations (extraction d'un nettoyage, formatage, mise en forme, visualisation, etc...). ces opérations de constitutions de datasets sont recensés dans les [scripts](./scripts)

Les datasets sont recensés ici et rangé en fonction du contexte, typologie et format (données tabulaires, arbres, etc...)

## Les textes de lois
Le projet de loi à ces différents stades de propositions

* L'avant-projet de loi soumis à consultation du 26 septembre au 18 octobre 2015

[propositionV0](./sources/projet-de-loi-pour-une-republique-numerique)

* Le projet de loi transmis au conseil d'Etat le 6 novembre

[propositionV1](.sources/projet-de-loi-transmis-au-conseil-d-etat)

* Le projet de loi final

[propositionV2](./sources/2015-12-09%20Projet%20de%20loi%20pr%C3%A9sent%C3%A9%20en%20CM.doc)

* **Le projet de loi soumis à consultation via la plateforme**

* Le projet de loi discuté et amendé à l'assemblée nationale
[https://www.legifrance.gouv.fr/affichLoiPubliee.do?idDocument=JORFDOLE000031589829&type=general&legislature=14](https://www.legifrance.gouv.fr/affichLoiPubliee.do?idDocument=JORFDOLE000031589829&type=general&legislature=14)


* Le projet de loi au sénat
[https://www.senat.fr/espace_presse/actualites/201603/projet_de_loi_pour_une_republique_numerique.html](https://www.senat.fr/espace_presse/actualites/201603/projet_de_loi_pour_une_republique_numerique.html)

## Les données issus du site de consultation

### Détails contributions sur le site en **OPEN DATA**
Mise à disposition sur le site de data.gouv.fr

Disponible [http://www.data.gouv.fr/fr/datasets/consultation-sur-le-projet-de-loi-republique-numerique/](https://www.data.gouv.fr/fr/datasets/consultation-sur-le-projet-de-loi-republique-numerique/)

Et au [téléchargement par ici](https://framagit.org/repnum/wiki/blob/master/datasets/projet-de-loi-numerique-consultation.csv)

Quatre types de contributions étaient possibles sur la plateforme :
1. propositions (ajout de nouveaux articles),
2. modification (proposition de modification d’articles),
3. arguments, sources, etc (présentés en colonne H),
4. vote.

###<a href="ref_a"> Référentiel des articles</a>
Disponible dans le dossier `articles/`
https://framagit.org/repnum/wiki/blob/master/datasets/articles/

Un [fichier CSV](https://framagit.org/repnum/wiki/blob/master/datasets/articles/referentiel_articles.csv) qui présente toutes les articles de loi avec leur :
* article_id, id de référence unique
* article_link, l'url qui correspond à la page de l'article proposé à la discussion, a modification et au vote
* title, le titre de l'article

Télécharger le [référentiel_article.csv] (https://framagit.org/repnum/wiki/blob/master/datasets/articles/referentiel_articles.csv)

:information_source: Pour générer un fichier à plat de nombreux scripts sont disponibles


###<a href="ref_p">Référentiel des participants
Tous les informations par participants disponibles dans le dossier `/participants`
Un [fichier CSV](https://framagit.org/repnum/wiki/blob/master/datasets/participants/participants.csv)
 qui présente chaque participants loi avec leur :
* username
* user_profile
* le nombre de votes (votes_total)
* le nombre de contributions (contributions_count)

:warning: attention le nombre de contributions ne prend pas en compte les
**propositions d'articles**
seulement le décompte des amendements (modifications) des arguments et des sources


### Articles du projet de loi

* Référentiel des articles du projet de loi:

`id de l'article,
url,
titre,
section
`

[référentiel_article.csv](../blob/master/datasets/referentiel_articles.csv)


* Articles du projet de loi

  * Un dossier qui contient chaque article référencés par id en JSON


[articles](https://framagit.org/repnum/wiki/blob/master/datasets/articles/)

  * L'ensemble des articles
[article.json](../blob/master/datasets/articles/articles.json)


Ce Jeu de données sous format json [articles.json](https://framagit.org/repnum/wiki/blob/master/datasets/articles/articles.json)

Il contient l'intégralité des articles proposés par le gouvernement
et par les participants avec le détail des contributions


Un article est defini par :
* son id (article_id)
* l'url de la page (article_link)
* son titre (title)
* son texte (body)
* le titre de section auquel il appartient (section_title)
* le titre du chapitre auquel il appartient (chapitre)
* l'auteur de l'article (author)
* l'explication de l'article (explication)
* la réponse du gouvernement (answer)

* date de creation (created_at)
* date de modification (updated_at)

quelques statistiques:

* le nombre de votes (votes_total)
* la repartition des votes (votes_ok,votes_nok, votes_mitigé)
* le nombre d'arguments (arguments_count)
* le nombre de versions (version_count)
* le nombre de sources (sources_count)

et comprend:

* la liste des sources
* la liste des versions
* la liste des arguments

:warning: le détails des votes n'est pas disponible

 :open_file_folder: **Télécharger** le [fichier articles.json] (https://framagit.org/repnum/wiki/blob/master/datasets/articles/articles.json
)


### Participants

* [référentiel_participants.csv](../blob/master/datasets/referentiel_participants.csv)

 * Un dossier qui contient chaque participants référencés par id en JSON




[participants](../blob/master/sources/participants)

* * Un fichier JSON qui contient tous les participants


`
participants": [
        {},
        ...
]
`

[participants](../blob/master/sources/participants.json)

* Jeu de données sous format json compressé au format zip [participants.zip](https://framagit.org/repnum/wiki/blob/master/datasets/participants/participants.zip
)

Il contient l'intégralité des participants à la plateforme et leurs contributions.


Une liste "participants" qui regroupe tous les participants e

 :open_file_folder: **Télécharger** le [fichier complet des participants] (https://framagit.org/repnum/wiki/blob/master/datasets/participants/participants.json)    

Un participant est defini par:
* son identifiant (username)
* un lien vers la page de son profile (user_profile)

quelques statistiques:

* contributions_count
* votes_count
* arguments_count
* versions_count
* sources_count

et comprend:

* la liste des votes
* la liste des sources
* la liste des modifications
* la liste des arguments


### Détails des arguments
Tous les arguments sont disponibles dans le dossier [arguments du projet] (https://framagit.org/repnum/wiki/blob/master/datasets/arguments/)

Il s'agit des arguments enoncés lors d'un vote

### Détails des votes

Les Opinion émises sur un article ou une *version d'article* sont disponibles dans le dossier opinions/


### <a href="#versions">Détails de modifications /versions
Modification faites sur l'article sont disponibles dans le dossier versions/

### Détails des sources
les sources proposés pour un article
sont disponible par:
* article:  dans article/articles.json

* participant: dans participants/participants.json
une version simplifiée des sources est stockées dans un fichier csv sources.csv

### Détails des réponses
Réponses données par le gouvernement  "answer" sont disponibles par article:
articles/articles.json
ou dans un fichier reponses.csv

### Historique
Dans le dossier historique vous avez la vision de toutes les actions sur la plateforme par date
* Vue consolidée pour chaque action par date
* Vue des votes par date

:information_source: Pour générer un fichier à plat de nombreux scripts sont disponibles et de nombreux datasets déjà prêts ;)
