Bienvenue sur l'espace de documentation du Sprint Rep Num

Vous trouverez ici toutes les informations nécessaires au bon déroulé du sprint.


##  Informations générales
* [Présentation](.presentation_générale)
* [Mentors](.mentors) l'équipe encadrante (méthodo + technique)

## Fonctionnement:
Voir le [mode d'emploi](.mode_emploi) qui vous explique par où commencer
comment fonctionne la plateforme collaborative du sprint.

## Les données à disposition
Parmi les données que nous mettons à votre disposition 
on distingue:
* les [sources](sources) (données brutes)
* des [datasets](datasets) (données déjà extraites)

qui couvrent le périmêtre que nous nous proposons d'étudier avec vous à savoir celle de la consultation citoyenne 
pour le projet de loi "Pour une république numérique"

> Si vous aviez besoin d'extraire des données supplémentaires pour les analyser cela peut couvrir le 4 jours du sprint
 
## Projets et groupes de recherche
La liste des projets et groupes de recherche

 * Equipe de test [WIKI du projet Groupe test](https://framagit.org/repnum/groupe_test/wikis/home)
 * :warning: **Ajouter ici votre groupe avec le lien vers le WIKI!!!!** Prenez exemple sur l'équipe test

## Appui technique et méthodologique
* la [Boite à outils du chercheur numérique](./outils)
Tous les outils que vous serez amenés à utiliser et plus encore...
Les installations et pré-requis techniques

* Les [Ateliers](ateliers)méthodologiues et techniques proposés pendant la durée du Sprint

## [Précédents](./précedents

Les études sur la loi république numériques  
sont nombreuses. Nous recensons une partie des initiatives autour de ce projet de loi
dans la [page correspondante](./précédents)

#### [Bibliographie](.biblio.md) [à compléter]
* travaux précédents des équipes et participants de HackRepNum
